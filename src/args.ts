import { Args } from "grimoire-kolmafia";

// TODO - Remove this for a much simpler version, as this isn't a public use script

export const args = Args.create("loopu", "Modified by (#1884174)", {
  version: Args.flag({
    help: "Output script version number and exit.",
    default: false,
    setting: "",
  }),
  mode: Args.string({
    help: "What mode to run? Optional, just type the task",
    default: "all",
  }),
  //Engine settings
  actions: Args.number({
    help: "Maximum number of actions to perform, if given. Can be used to execute just a few steps at a time.",
  }),
  abort: Args.string({
    help: "If given, abort during the prepare() step for the task with matching name.",
  }),
  debug: Args.flag({
    help: "Run in debug mode",
    default: false,
    setting: "",
  }),
  organ: Args.flag({
    help: "Grab steel organ",
    default: false,
  }),
  quick: Args.boolean({
    help: "Run scripts quickly",
    default: false,
  }),
  pvp: Args.flag({
    help: "If true, break hippy stone and do pvp.",
    default: false,
  }),
  csscript: Args.string({
    help: "CS script to run",
    default: "instantsccs",
    // default: "folgercs",
  }),
  garboaftercore: Args.string({
    help: "Aftercore Garbo command",
    default: "garbo candydish ascend ",
  }),
  garboaftercoreDrunk: Args.string({
    help: "Dunk Garbo pre CS command",
    default: "garbo candydish ascend yachtzeechain ",
  }),
  garboPostLoop: Args.string({
    help: "Garbo post CS command",
    default: "garbo candydish yachtzeechain ",
  }),
  smol: Args.string({
    help: "Custom smol command",
    default: "loopsmol",
  }),
  robot: Args.string({
    help: "Custom robot command",
    default: "looprobot warby=200 ",
  }),
  voa: Args.string({
    help: "VOA for Garbo",
    default: "6150",
  }),
  voaDrunk: Args.string({
    help: "VOA while drunk",
    default: "4500",
  }),
  clan: Args.string({
    default: "Margaretting Tye",
  }),
});
