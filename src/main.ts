import { Args, getTasks } from "grimoire-kolmafia";
import { print } from "kolmafia";
import { args } from "./args";
import { Engine } from "./engine/engine";
import { Aftercore } from "./tasks/aftercore/aftercore";
// import { Crimbo, CrimboPost } from "./tasks/aftercore/crimbo24";
import { LongAftercore } from "./tasks/aftercore/longaftercore";
import { CSAll } from "./tasks/loops/cs";
import { robotAll } from "./tasks/loops/robot";
import { smolAll } from "./tasks/loops/smol";
import { standardAll } from "./tasks/loops/standard";
import { Sloppy } from "./tasks/misc/sbb";
import { checks } from "./tasks/pcheck";
import type { Task } from "./tasks/structure";
import { test } from "./tasks/test";
import { cliExecuteThrow } from "./tasks/utils";

const version = "1.3.3";
const currentFarming = "robot";

// Main loop/task table
const taskTable = {
  aftercore: getTasks([Aftercore()]),
  all: getTasks([Aftercore(), ...robotAll()]),
  cs: getTasks([Aftercore(), ...CSAll()]),
  long: getTasks([LongAftercore()]),
  profit: [],
  robot: getTasks([Aftercore(), ...robotAll()]),
  sloppy: getTasks([Sloppy()]),
  smol: getTasks([Aftercore(), ...smolAll()]),
  standard: getTasks([Aftercore(), ...standardAll()]),
  // crimbo2: getTasks([Crimbo(), ...justRobot(), CrimboPost()]),
  // crimbo: getTasks([Crimbo()]),
};

// Run functions
const functionTable = {
  pcheck: checks,
  test: test,
};

// Adhoc CLI tasks, for events or one offs
const cliTasks = {
  chrono: [
    "ptrack add loopu",
    "breakfast",
    "CONSUME VALUE 10000 ALL",
    "garbo nobarf nodiet",
    "chrono mode=soup",
    // "loopu mode=long quick=true",
    // "loopu mode=smol",
  ],
  chrono2: [
    "CONSUME VALUE 10000 ALL",
    "garbo nobarf nodiet",
    "chrono",
    "loopu mode=smol",
  ],
  detail: ["ptrack dcompare loopu loopu_end"],
  ween: [
    "ptrack add loopu",
    "breakfast",
    "CONSUME ALL VALUE 10000",
    "garbo nobarf nodiet",
    "set freecandy_familiar=Red-Nosed Snapper",
    "freecandy",
  ],
};

// biome-ignore lint/complexity/noExcessiveCognitiveComplexity: <explanation>
export function main(scriptCommand = currentFarming) {
  let command = scriptCommand;
  if (!command.startsWith("mode=")) {
    command = `mode=${scriptCommand}`;
  }

  Args.fill(args, command);

  if (args.help) {
    print(`loopu v${version}`);
    Args.showHelp(args);
    return 0;
  }

  print(`Running: loopu v${version}`);
  let tasks: Task[];
  const runMode = args.mode;

  if (runMode in functionTable) {
    functionTable[runMode]();
    return 0;
  }

  if (runMode in cliTasks) {
    const instructions = cliTasks[runMode];
    for (const instruction of instructions) {
      cliExecuteThrow(instruction);
    }
    return 0;
  }

  if (runMode in taskTable) {
    tasks = taskTable[runMode];
  } else {
    throw new Error("Unknown command");
  }

  if (tasks === null || tasks === undefined) {
    throw new Error("Somehow ended up with an empty task list");
  }

  // Modify all garbo commands
  if (args.quick) {
    args.garboaftercore += " quick";
    args.garboaftercoreDrunk += " quick";
    args.garboPostLoop += " quick";
  }

  // Abort during the prepare() step of the specified task
  if (args.abort) {
    const toAbort = tasks.find((task) => task.name === args.abort);
    if (!toAbort) {
      throw new Error(`Unable to identify task ${args.abort}`);
    }
    toAbort.prepare = (): void => {
      throw new Error("Abort requested");
    };
  }

  const engine = new Engine(tasks, [], "loopu tracker");
  if (tasks.length === 0) {
    engine.destruct();
    return 0;
  }

  print(`Running ${tasks.length} tasks`);
  try {
    engine.run(args.actions);
    // Print the next task that will be executed, if it exists
    const task = engine.getNextTask();
    if (task) {
      print(`Next: ${task.name}`, "olive");
    }

    // If the engine ran to completion, all tasks should be complete.
    // Print any tasks that are not complete.
    if (args.actions === undefined) {
      const uncompletedTasks = engine.tasks.filter((t) => !t.completed());
      if (uncompletedTasks.length > 0) {
        print("Uncompleted Tasks:");
        for (const t of uncompletedTasks) {
          print(t.name);
        }
      }
    }
  } finally {
    engine.destruct();
  }

  return 0;
}
