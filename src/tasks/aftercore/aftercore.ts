import {
  cliExecute,
  getClanName,
  myAdventures,
  myHp,
  myMaxhp,
  pvpAttacksLeft,
  restoreHp,
  use,
} from "kolmafia";
import { $item, get, have, set } from "libram";
import { args } from "../../args";
import {
  beach,
  breakStone,
  clipArt,
  distillate,
  glitch,
  nightcap,
  pullAll,
  sausage,
  scepterProfit,
  startPtrack,
  swagger,
} from "../common";
import { Leg, type Quest, getCurrentLeg } from "../structure";
import {
  canConsume,
  cliExecuteThrow,
  stooperDrunk,
  totallyDrunk,
} from "../utils";

const target = "'pumpkin spice wraith'";

export function Aftercore(): Quest {
  return {
    name: "Aftercore",
    completed: () =>
      (myAdventures() === 0 && totallyDrunk() && pvpAttacksLeft() === 0) ||
      getCurrentLeg() !== Leg.PreRun,
    tasks: [
      pullAll(),
      startPtrack(),
      {
        name: "Join VIP Clan",
        completed: () =>
          !args.clan || getClanName().toLowerCase() === args.clan.toLowerCase(),
        do: () => cliExecute(`/whitelist ${args.clan}`),
      },
      {
        name: "Breakfast",
        completed: () => get("breakfastCompleted"),
        do: () => cliExecute("breakfast"),
        tracking: "Breakfast",
      },
      // ...getDNA(true),
      {
        name: "SIT Course",
        ready: () => have($item`S.I.T. Course Completion Certificate`),
        completed: () => get("_sitCourseCompleted", false),
        choices: {
          1494: 2,
        },
        do: () => use($item`S.I.T. Course Completion Certificate`),
      },
      clipArt(),
      glitch(),
      beach(),
      scepterProfit(true, 5),
      distillate(), // Shotglass
      breakStone(args.pvp),
      {
        name: "Garbo",
        ready: () => myAdventures() > 0,
        completed: () =>
          (get("_garboCompleted", "") !== "" &&
            myAdventures() === 0 &&
            !canConsume()) ||
          stooperDrunk(),
        prepare: () => {
          if (!get("_floundryItemCreated")) {
            cliExecuteThrow("acquire carpe");
          }
          if (myHp() < 100) {
            restoreHp(myMaxhp());
            // cliExecute("BeachComber free rare");
          }
        },
        do: () => {
          set("_garbageItemChanged", true);
          set("valueOfAdventure", args.voa);
          cliExecute("BeachComber free rare");
          cliExecute(`${args.garboaftercore} target=${target}`);
        },
        tracking: "garbo",
        limit: { tries: 5 },
      },
      sausage(23),
      nightcap(args.voaDrunk),
      {
        name: "Garbo Drunk",
        completed: () => myAdventures() === 0 && totallyDrunk(),
        ready: () => myAdventures() !== 0 && totallyDrunk(),
        do: () => {
          set("valueOfAdventure", args.voaDrunk);
          cliExecute(`${args.garboaftercoreDrunk} target=${target}`);
        },
        tracking: "garbo drunk",
        limit: { tries: 1 },
      },
      swagger(args.pvp),
    ],
  };
}
