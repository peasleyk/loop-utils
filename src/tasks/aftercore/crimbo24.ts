// import { getClanName, myMaxhp, restoreHp, use } from "kolmafia";
// import {
//   canInteract,
//   cliExecute,
//   myAdventures,
//   numericModifier,
//   print,
//   visitUrl,
// } from "kolmafia";
// import { $item, have } from "libram";
// import { $effect, get, questStep, set, uneffect } from "libram";
// import { args } from "../../args";
// import { clipArt, pullAll, scepterProfit, startPtrack } from "../common";
// import {
//   beach,
//   breakStone,
//   breakfast,
//   endPtrack,
//   glitch,
//   moon,
//   nightcap,
//   pajamas,
//   sausage,
// } from "../common";
// import { ensureAdventures } from "../loops/robot";
// import { uneat } from "../loops/smol";
// import { Leg, type Quest, getCurrentLeg } from "../structure";
// import { canConsume, setRef } from "../utils";
// import { doneAdventuring, getRef, stooperDrunk, totallyDrunk } from "../utils";

// const island = "veteransday";
// // const island = "thanksgiving";

// export function Crimbo(): Quest {
//   return {
//     name: "Crimbo Aftercore",
//     completed: () =>
//       (myAdventures() === 0 && !canConsume()) || getCurrentLeg() !== Leg.PreRun,
//     tasks: [
//       pullAll(),
//       startPtrack(),
//       {
//         name: "Join VIP Clan",
//         completed: () =>
//           !args.clan || getClanName().toLowerCase() === args.clan.toLowerCase(),
//         do: () => cliExecute(`/whitelist ${args.clan}`),
//       },
//       {
//         name: "Breakfast",
//         completed: () => get("breakfastCompleted"),
//         do: () => cliExecute("breakfast"),
//         tracking: "Breakfast",
//       },
//       // ...getDNA(true),
//       {
//         name: "SIT Course",
//         ready: () => have($item`S.I.T. Course Completion Certificate`),
//         completed: () => get("_sitCourseCompleted", false),
//         choices: {
//           1494: 2,
//         },
//         do: () => use($item`S.I.T. Course Completion Certificate`),
//       },
//       clipArt(),
//       glitch(),
//       beach(),
//       scepterProfit(true, 5),
//       breakStone(args.pvp),
//       {
//         name: "Garbo nobarf",
//         ready: () => getRef("crimboGarbo1") === "",
//         completed: () => getRef("crimboGarbo1") === "Done",
//         do: () => {
//           cliExecute("CONSUME ALLOWLIFETIMELIMITED ALL VALUE 10000");
//           const res = cliExecute(
//             "garbo nobarf nodiet ascend target='Section 11'",
//           );
//           if (res === true) {
//             setRef("crimboGarbo1", "Done");
//           }
//         },
//         clear: "all",
//         tracking: "garbo",
//         limit: { tries: 2 },
//       },
//       {
//         name: "Crimbo",
//         // ready: () => myAdventures() > 0,
//         completed: () =>
//           (myAdventures() === 0 && !canConsume()) || stooperDrunk(),
//         ready: () => getRef("crimboGarbo1") === "Done" && myAdventures() > 0,
//         prepare: () => {
//           uneffect($effect`Beaten Up`);
//           restoreHp(myMaxhp());
//         },
//         tracking: "crimbo",
//         // limit: { tries: 2 },
//         do: (): void => {
//           // cliExecute("autoattack crimbo_yaho");
//           cliExecute(`crimbo island=${island} ascend`);
//         },
//         limit: { tries: 2 },
//       },
//     ],
//   };
// }

// export function CrimboPost(): Quest {
//   return {
//     name: "Second Crimbo  Aftercore",
//     // completed: () =>
//     // (hasAscended() &&
//     // totallyDrunk() &&
//     // inAscension(),
//     completed: () => totallyDrunk() && getRef("loopu_ptrackEnded") === "true",
//     ready: () => getCurrentLeg() === Leg.PostRun,
//     tasks: [
//       {
//         name: "Break Prism",
//         ready: () =>
//           questStep("questL13Final") === 13 ||
//           get("questL13Final") === "step13",
//         completed: () => canInteract(),
//         do: () => {
//           visitUrl("place.php?whichplace=nstower&action=ns_11_prism");
//           cliExecute("refresh all");
//           set("_thesisDelivered", false);
//         },
//         tracking: "ignore",
//       },
//       breakfast(),
//       uneat(),
//       ensureAdventures(),
//       // ...OrganQuest,
//       breakStone(args.pvp),
//       beach(),
//       glitch(),
//       moon(""),
//       // ...getDNA(true, "model train set"),
//       // tofu(), // Garbo buys now
//       // clipArt(false), // Garbo will handle this
//       // scepterProfit(true, 3), // Save for rollover
//       // distillate(), // Shotglass
//       {
//         name: "Garbo nobarf",
//         ready: () => getRef("crimboGarbo2") === "",
//         completed: () => getRef("crimboGarbo2") === "Done",
//         do: () => {
//           cliExecute("CONSUME ALLOWLIFETIMELIMITED ALL VALUE 10000");
//           cliExecute("garbo nobarf nodiet target='Section 11'");
//           setRef("crimboGarbo2", "Done");
//         },
//         clear: "all",
//         tracking: "garbo",
//         limit: { tries: 2 },
//       },
//       {
//         name: "Crimbo",
//         // ready: () => myAdventures() > 0,
//         completed: () => myAdventures() === 0 || stooperDrunk(),
//         prepare: () => {
//           uneffect($effect`Beaten Up`);
//           restoreHp(myMaxhp());
//         },
//         tracking: "crimbo",
//         // limit: { tries: 2 },
//         do: (): void => {
//           // cliExecute("autoattack crimbo24");
//           cliExecute(`crimbo island=${island}`);
//         },
//         limit: { tries: 2 },
//       },
//       sausage(23),
//       nightcap(args.voaDrunk),
//       pajamas(),
//       endPtrack("loopu_end"),
//       {
//         name: "Alert-No Nightcap",
//         ready: () => !doneAdventuring(),
//         completed: () => stooperDrunk(),
//         do: (): void => {
//           const targetAdvs = 100 - numericModifier("adventures");
//           print("loopu completed, but did not overdrink.", "red");
//           if (targetAdvs < myAdventures() && targetAdvs > 0) {
//             print(
//               `Rerun with fewer than ${targetAdvs} adventures for loopu to handle your diet`,
//               "red",
//             );
//           } else {
//             print("Something went wrong.", "red");
//           }
//         },
//       },
//     ],
//   };
// }
