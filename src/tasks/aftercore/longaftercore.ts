import {
  cliExecute,
  inebrietyLimit,
  myAdventures,
  myInebriety,
  myTurncount,
  pvpAttacksLeft,
} from "kolmafia";
import { get, set } from "libram";
import { args } from "../../args";
import {
  distillate,
  endPtrack,
  glitch,
  nightcap,
  pajamas,
  sausage,
  swagger,
  tofu,
} from "../common";
import type { Quest } from "../structure";
import { getRef, setRef, totallyDrunk } from "../utils";

export function LongAftercore(): Quest {
  return {
    name: "Aftercore",
    tasks: [
      glitch(),
      {
        name: "Breakfast",
        completed: () => get("breakfastCompleted"),
        do: () => cliExecute("breakfast"),
        tracking: "Breakfast",
      },
      tofu(),
      {
        name: "Garbo",
        prepare: () => cliExecute("acquire carpe"),
        do: (): void => {
          setRef("garboFarmingTurns", myTurncount());
          set("valueOfAdventure", args.voa);
          cliExecute(args.garboaftercore);
        },
        completed: () =>
          myAdventures() === 0 || myInebriety() > inebrietyLimit(),
        tracking: "Garbo",
      },
      {
        name: "Stop garbo Tracking",
        ready: () => totallyDrunk(),
        completed: () => getRef("garboFarmingTurns") !== 0,
        do: () => {
          const currentTurns = Number(get("garboFarmingTurns"));
          setRef("garboFarmingTurns", myTurncount() - currentTurns);
        },
      },
      sausage(),
      swagger(),
      distillate(),
      nightcap(args.voaDrunk),
      pajamas(),
      endPtrack("loopu_end"),
    ],
    completed: () =>
      pvpAttacksLeft() === 0 && totallyDrunk() && myAdventures() === 0,
  };
}
