import {
  cliExecute,
  inebrietyLimit,
  myAdventures,
  myInebriety,
  numericModifier,
  print,
} from "kolmafia";
import { $effect, set, uneffect } from "libram";
import { args } from "../../args";
import {
  beach,
  breakStone,
  breakfast,
  clipArt,
  endPtrack,
  glitch,
  moon,
  nightcap,
  pajamas,
  sausage,
  scepterProfit,
} from "../common";
import { Leg, type Quest, getCurrentLeg } from "../structure";
import {
  canDiet,
  doneAdventuring,
  getRef,
  stooperDrunk,
  totallyDrunk,
} from "../utils";

/*

Shared post run tasks between loops

*/

const target = "'pumpkin spice wraith'";

export function postLoop(): Quest {
  return {
    name: "Second Aftercore",
    completed: () => totallyDrunk() && getRef("loopu_ptrackEnded") === "true",
    ready: () => getCurrentLeg() === Leg.PostRun,
    tasks: [
      breakfast(),
      // ensureAdventures(),
      breakStone(args.pvp),
      beach(),
      glitch(),
      moon(""),
      // ...getDNA(true, "model train set"),
      // tofu(), // Garbo buys now
      clipArt(false), // Garbo will handle this
      scepterProfit(true, 3), // Save for rollover
      {
        name: "Garbo",
        ready: () => myInebriety() <= inebrietyLimit() && myAdventures() > 0,
        completed: () => (myAdventures() === 0 && !canDiet()) || stooperDrunk(),
        prepare: () => uneffect($effect`Beaten Up`),
        do: () => {
          set("valueOfAdventure", args.voa);
          cliExecute("use wardrobe-o-matic");
          cliExecute("BeachComber free rare");
          cliExecute(`${args.garboPostLoop} target=${target}`);
        },
        clear: "all",
        tracking: "garbo",
        limit: { tries: 2 },
      },
      sausage(), // Will likely not be used
      nightcap(),
      pajamas(),
      endPtrack("loopu_end"),
      {
        name: "Alert-No Nightcap",
        ready: () => !doneAdventuring(),
        completed: () => stooperDrunk(),
        do: (): void => {
          const targetAdvs = 100 - numericModifier("adventures");
          print("loopu completed, but did not overdrink.", "red");
          if (targetAdvs < myAdventures() && targetAdvs > 0) {
            print(
              `Rerun with fewer than ${targetAdvs} adventures for loopu to handle your diet`,
              "red",
            );
          } else {
            print("Something went wrong.", "red");
          }
        },
      },
    ],
  };
}
