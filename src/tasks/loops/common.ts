import { step } from "grimoire-kolmafia";
import {
  type Class,
  type Item,
  type Path,
  cliExecute,
  create,
  getWorkshed,
  myAdventures,
  myPath,
  print,
  runChoice,
  toInt,
  toItem,
  visitUrl,
} from "kolmafia";
import {
  $path,
  Lifestyle,
  type MoonSign,
  ascend,
  get,
  have,
  prepareAscension,
  set,
} from "libram";
import { moon, pullAll } from "../common";
import { Leg, type Quest, type Task, getCurrentLeg } from "../structure";
import { acquire, cliExecuteThrow, getPerms, totallyDrunk } from "../utils";

/*
  Shared functions for prepping and kicking off a run
*/

const gardens = ["Peppermint Pip Packet", "packet of rock seeds"] as const;
type Garden = (typeof gardens)[number];

export type RunSettings = {
  path: Path;
  class: Class;
  moon: MoonSign;
  pet: Item;
  consumable: Item;
  garden: Garden;
  // TODO implement these
  preWorkshed?: string;
  postWorkshed?: string;
  moonspoon?: MoonSign;
};

export type PullSettings = {
  pulls: Item[];
  crafts: Item[];
  buys: Item[];
};

export function preWorkshed(runSettings: RunSettings): Task {
  let skip = false;
  if (runSettings.preWorkshed === undefined) {
    skip = true;
  }
  const task: Task = {
    name: "Switch Workshed Pre Run",
    completed: () => {
      if (skip) {
        return true;
      }
      return getWorkshed() === toItem(runSettings.preWorkshed);
    },
    ready: () => getCurrentLeg() === Leg.PreRun || myAdventures() === 0,
    do: (): void => {
      if (runSettings.preWorkshed) {
        cliExecute(`use ${runSettings.preWorkshed}`);
      }
    },
  };
  return task;
}

export function postWorkshed(runSettings: RunSettings): Task {
  let skip = false;
  if (runSettings.postWorkshed === undefined) {
    skip = true;
  }
  const task: Task = {
    name: "Switch Workshed Post Run",
    completed: () => {
      if (skip) {
        return true;
      }
      return getWorkshed() === toItem(runSettings.postWorkshed);
    },
    ready: () =>
      getCurrentLeg() === Leg.PostRun &&
      getWorkshed() !== toItem(runSettings.postWorkshed),
    do: (): void => {
      if (runSettings.postWorkshed) {
        print("using post workshed");
        cliExecute(`use ${runSettings.postWorkshed}`);
      }
    },
  };
  return task;
}

export function pullItems(pullSettings: PullSettings): Task[] {
  const items = pullSettings.pulls.concat(pullSettings.crafts);
  return items.map((i) => {
    const task: Task = {
      name: "Pull Crap",
      ready: () => have(i) && getCurrentLeg() === Leg.InRun,
      completed: () =>
        have(i) ||
        get("_roninStoragePulls")
          .split(",")
          .map((id) => toItem(toInt(id)))
          .includes(i),
      do: () => cliExecuteThrow(`pull ${i}`),
      tracking: "Ignore",
    };
    return task;
  });
}

export function acquirePullItems(pullSettings: PullSettings): Task {
  const allPulls = pullSettings.pulls
    .concat(pullSettings.crafts)
    .concat(pullSettings.buys);

  if (
    allPulls.length > 20 ||
    (myPath() === $path`Community Service` && allPulls.length > 5)
  ) {
    throw new Error("Too many items to pull!");
  }

  return {
    name: "Get Items",
    completed: () => {
      return (
        allPulls.filter((item) => have(item)).length === allPulls.length ||
        getCurrentLeg() === Leg.InRun
      );
    },
    do: (): void => {
      for (const item of pullSettings.buys) {
        if (!have(item)) {
          acquire(1, item);
        }
      }
      for (const item of pullSettings.crafts) {
        if (!have(item)) {
          create(item, 1);
        }
      }
    },
    tracking: "Acuire pulls",
  };
}

export function freeKing(): Task {
  return {
    name: "Free King",
    ready: () => step("questL13Final") === 13,
    completed: () => get("kingLiberated", false),
    do: (): void => {
      visitUrl("place.php?whichplace=nstower&action=ns_11_prism");
    },
    clear: "all",
    tracking: "ignore",
  };
}
/*
  Switch to a workshed, or ignore if we're already there
*/
export function prep(
  runSettings: RunSettings,
  pullSettings: PullSettings,
): Quest {
  const allPulls = pullSettings.pulls
    .concat(pullSettings.crafts)
    .concat(pullSettings.buys);

  const workshedConditional = true;

  return {
    name: runSettings.path.name,
    completed: () => getCurrentLeg() !== Leg.PreRun,
    ready: () =>
      getCurrentLeg() === Leg.PreRun && myAdventures() === 0 && totallyDrunk(),
    tasks: [
      acquirePullItems(pullSettings),
      {
        name: "Visit Council",
        completed: () => get("_loopuCouncilVisited", true),
        do: (): void => {
          visitUrl("council.php");
          set("_loopuCouncilVisited", true);
        },
      },
      preWorkshed(runSettings),
      {
        name: `Ascend ${runSettings.path.name}`,
        completed: () => getCurrentLeg() !== Leg.PreRun,
        ready: () =>
          allPulls.filter((item) => have(item)).length >= allPulls.length,
        do: (): void => {
          const perms = getPerms();
          prepareAscension({
            garden: runSettings.garden,
          });
          ascend({
            path: runSettings.path,
            playerClass: runSettings.class,
            lifestyle: Lifestyle.softcore,
            moon: runSettings.moon,
            consumable: runSettings.consumable,
            pet: runSettings.pet,
            permOptions: { permSkills: perms, neverAbort: false },
          });
          set("choiceAdventure1446", 1);
          if (
            visitUrl("main.php").includes(
              "one made of rusty metal and scrap wiring",
            )
          ) {
            runChoice(-1);
          }
          cliExecute("refresh all");
        },
        tracking: "Ignore",
      },
      ...pullItems(pullSettings),
    ],
  };
}

// Tasks to run at the end of any loop
export function post(runSettings: RunSettings): Task[] {
  return [pullAll(), postWorkshed(runSettings), moon(runSettings.moonspoon)];
}
