import { step } from "grimoire-kolmafia";
import {
  cliExecute,
  equip,
  itemAmount,
  myAscensions,
  use,
  visitUrl,
} from "kolmafia";
import {
  $class,
  $familiar,
  $familiars,
  $item,
  $path,
  get,
  have,
  set,
} from "libram";
import { args } from "../../args";
import { postLoop } from "../aftercore/post";
import { pullAll } from "../common";
import { Leg, type Quest, getCurrentLeg } from "../structure";
import { setRef } from "../utils";
import { type PullSettings, type RunSettings, prep } from "./common";

// Everything we should pull for CS
const runSettings: RunSettings = {
  path: $path`Community Service`,
  class: $class`Sauceror`,
  moon: "Blender",
  pet: $item`astral chapeau`,
  consumable: $item`astral six-pack`,
  garden: "Peppermint Pip Packet",
  preWorkshed: "Model Train Set",
};

const pullSettings: PullSettings = {
  pulls: [
    $item`Buddy Bjorn`,
    $item`crown of thrones`,
    $item`Great Wolf's beastly trousers`,
  ],
  crafts: [$item`Pizza of Legend`],
  buys: [],
};

export function csRun(runSettings: RunSettings): Quest {
  return {
    name: runSettings.path.name,
    completed: () =>
      getCurrentLeg() === Leg.PostRun && step("questL13Final") > 11,
    ready: () => getCurrentLeg() === Leg.InRun,
    tasks: [
      {
        name: "Refresh All",
        completed: () => get("_loopuRefreshed", false),
        do: () => {
          cliExecute("refresh all");
          set("_loopuRefreshed", true);
        },
      },
      {
        name: "Prep Fireworks Shop",
        completed: () =>
          !have($item`Clan VIP Lounge key`) ||
          get("_loopuFireworksPrepped", false),
        do: () => {
          visitUrl("clan_viplounge.php?action=fwshop&whichfloor=2");
          set("_loopuFireworksPrepped", true);
        },
      },
      {
        name: "SIT Course",
        ready: () => have($item`S.I.T. Course Completion Certificate`),
        completed: () => get("_sitCourseCompleted", false),
        choices: {
          1494: 2,
        },
        do: () => use($item`S.I.T. Course Completion Certificate`),
      },

      {
        name: "Stillsuit Prep",
        completed: () => itemAmount($item`tiny stillsuit`) === 0,
        do: () =>
          equip(
            $item`tiny stillsuit`,
            get(
              "stillsuitFamiliar",
              $familiars`Gelatinous Cubeling`.find((fam) => have(fam)) ||
                $familiar`none`,
            ),
          ),
      },
      {
        name: "Run",
        completed: () => get("lastEmptiedStorage") === myAscensions(),
        do: () => {
          // Tested
          // Batteries?
          set("instant_saveEuclideanAngle", true);
          set("instant_savePerfectFreeze", true);
          set("instant_saveRichRicotta", true);
          set("instant_saveRicottaCasserole", true);
          set("instant_saveRoastedVegetableItem", true);
          set("instant_saveRoastedVegetableStats", true);
          set("instant_saveSacramentoWine", true);
          set("instant_saveWileyWheyBar", true);
          set("instant_saveBackups", 0);
          set("instant_saveAugustScepter", true);
          set("instant_skipCyclopsEyedrops", true);
          set("instant_skipGovernment", true);
          set("instant_skipEarlyTrainsetMeat", true);
          set("instant_skipDistilledFortifiedWine", true);

          // CBB legends
          set("_instant_skipDeepDishOfLegend", true);
          set("_instant_skipCalzoneOfLegend", true);
          set("_instant_skipPizzaOfLegend", true);

          // Adds turns
          set("instant_skipCabernetSauvignon", true);
          set("instant_skipSynthExp", false);

          // Adds turns, saves organs
          set("instant_saveSockdollager", true);

          //Testing..

          // Adds 2 turns?
          set("instant_skipAutomaticOptimizations", true);

          // Need to evaluate turn increase
          set("instant_saveLocketRedSkeleton", true);
          set("instant_savePlainCalzone", true);

          // Recheck every so often
          set("instant_mystTestTurnLimit", "10");
          set("instant_musTestTurnLimit", "10");

          // Start the script
          cliExecute(args.csscript);
          setRef("CSDone", true);
        },
        clear: "all",
        tracking: "run",
        limit: { tries: 2 },
      },
      pullAll(),
    ],
  };
}

export function CSAll(): Quest[] {
  return [prep(runSettings, pullSettings), csRun(runSettings), postLoop()];
}
export function JustCS(): Quest[] {
  return [prep(runSettings, pullSettings), csRun(runSettings)];
}
