import { step } from "grimoire-kolmafia";
import {
  cliExecute,
  drink,
  haveSkill,
  myAdventures,
  myLevel,
  myMaxhp,
  myPath,
  myStorageMeat,
  restoreHp,
  use,
  useSkill,
  visitUrl,
} from "kolmafia";
import {
  $class,
  $effect,
  $item,
  $path,
  $skill,
  get,
  have,
  set,
  uneffect,
} from "libram";
import { args } from "../../args";
import { postLoop } from "../aftercore/post";
import { mayam } from "../common";
import {
  Leg,
  type Quest,
  type Task,
  getCurrentLeg,
  hasAscended,
} from "../structure";
import { setRef } from "../utils";
import { type PullSettings, type RunSettings, post, prep } from "./common";

const runSettings: RunSettings = {
  path: $path`You, Robot`,
  // class: $class`Accordion Thief`,
  class: $class`Seal Clubber`,
  moon: "Vole",
  pet: $item`astral mask`,
  consumable: $item`astral six-pack`,
  garden: "Peppermint Pip Packet",
  postWorkshed: "model train set",
  // postWorkshed: "TakerSpace letter of Marque",
  moonspoon: "Blender",
};

const pullSettings: PullSettings = {
  pulls: [],
  crafts: [$item`Pizza of Legend`],
  buys: [],
};

export function ensureAdventures(): Task {
  // Robot might end up with less than 10 advs
  return {
    name: "Ensure Advs",
    after: ["Break Prism"],
    ready: () =>
      (myAdventures() < 10 && hasAscended()) ||
      myPath() === $path`A Shrunken Adventurer Am I`,
    completed: () =>
      myAdventures() >= 10 ||
      haveSkill($skill`Liver of Steel`) ||
      myPath() === $path`Community Service`,
    do: () => {
      useSkill($skill`Ode to booze`, 1);
      if (have($item`astral six-pack`)) {
        use($item`Astral six-pack`);
      }
      drink($item`astral pilsner`, 1);
    },
    tracking: "robot",
    limit: { tries: 1 },
  };
}

export function robotRun(runSettings: RunSettings): Quest {
  return {
    name: runSettings.path.name,
    completed: () => getCurrentLeg() === Leg.PostRun && myStorageMeat() === 0,
    ready: () => getCurrentLeg() === Leg.InRun || myStorageMeat() > 0,
    tasks: [
      {
        name: "Refresh All",
        completed: () => get("_loopuRefreshed", false),
        do: () => {
          cliExecute("refresh all");
          set("_loopuRefreshed", true);
          mayam();
        },
      },
      {
        name: "Prep Fireworks Shop",
        completed: () =>
          !have($item`Clan VIP Lounge key`) ||
          get("_loopuFireworksPrepped", false),
        do: () => {
          visitUrl("clan_viplounge.php?action=fwshop&whichfloor=2");
          set("_loopuFireworksPrepped", true);
        },
      },
      {
        name: "Acquire Mouthwash",
        completed: () =>
          // eslint-disable-next-line libram/verify-constants
          have($item`mmm-brr! brand mouthwash`) ||
          !have($item`Sept-Ember Censer`) ||
          myLevel() >= 10,
        do: (): void => {
          // Grab Embers
          visitUrl("shop.php?whichshop=september");

          // Grab Bembershoot
          visitUrl(
            "shop.php?whichshop=september&action=buyitem&quantity=1&whichrow=1516&pwd",
          );

          // Grab Mouthwashes
          visitUrl(
            "shop.php?whichshop=september&action=buyitem&quantity=3&whichrow=1512&pwd",
          );
        },
        limit: { tries: 1 },
      },
      {
        name: "Run",
        completed: () => step("questL13Final") === 13,
        prepare: () => {
          uneffect($effect`Beaten Up`);
          restoreHp(myMaxhp());
        },
        do: () => {
          set("looprobot_nuns", true);
          const ret = cliExecute(args.robot);
          if (ret === true) {
            setRef("robotDone", true);
          }
        },
        clear: "all",
        tracking: "run",
        limit: { tries: 5 },
      },
      {
        name: "Finish",
        after: ["Run"],
        completed: () => step("questL13Final") === 999,
        do: () => visitUrl("place.php?whichplace=nstower&action=ns_11_prism"),
        limit: { tries: 1 },
        tracking: "ignore",
      },
      ...post(runSettings),
    ],
  };
}

export function robotAll(): Quest[] {
  return [
    prep(runSettings, pullSettings),
    robotRun(runSettings),
    post(runSettings),
    postLoop(),
  ];
}

export function justRobot(): Quest[] {
  return [
    prep(runSettings, pullSettings),
    robotRun(runSettings),
    post(runSettings),
  ];
}
