import { step } from "grimoire-kolmafia";
import {
  cliExecute,
  drink,
  equip,
  itemAmount,
  myAdventures,
  myFullness,
  myInebriety,
  storageAmount,
  use,
  useSkill,
  visitUrl,
} from "kolmafia";
import {
  $class,
  $effect,
  $familiar,
  $familiars,
  $item,
  $path,
  $skill,
  get,
  getRemainingLiver,
  getRemainingStomach,
  have,
  set,
  uneffect,
} from "libram";
import { args } from "../../args";
import { postLoop } from "../aftercore/post";
import { mayam, pullAll } from "../common";
import { Leg, type Quest, type Task, getCurrentLeg } from "../structure";
import { acquire, getRef, setRef } from "../utils";
import { type PullSettings, type RunSettings, freeKing, prep } from "./common";

const runSettings: RunSettings = {
  path: $path`A Shrunken Adventurer Am I`,
  class: $class`Seal Clubber`,
  moon: "Vole",
  pet: $item`astral mask`,
  consumable: $item`astral six-pack`,
  garden: "Peppermint Pip Packet",
};

const pullSettings: PullSettings = {
  pulls: [],
  crafts: [
    $item`Pizza of Legend`,
    $item`Deep Dish of Legend`,
    $item`Calzone of Legend`,
  ],
  buys: [$item`Ol' Scratch's salad fork`, $item`Frosty's frosty mug`],
};

export function smolRun(runSettings: RunSettings): Quest {
  return {
    name: runSettings.path.name,
    completed: () =>
      // getCurrentLeg() === Leg.PostRun &&
      get("questL13Final") === "finished" && getRef("uneated") === "true",
    ready: () =>
      (getCurrentLeg() === Leg.PreRun && myAdventures() === 0) ||
      getCurrentLeg() === Leg.InRun ||
      getRef("uneated") !== "true",
    tasks: [
      {
        name: "Refresh All",
        completed: () => get("_loopuRefreshed", false),
        do: () => {
          cliExecute("refresh all");
          set("_loopuRefreshed", true);
          mayam();
        },
      },
      {
        name: "Prep Fireworks Shop",
        completed: () =>
          !have($item`Clan VIP Lounge key`) ||
          get("_loopuFireworksPrepped", false),
        do: () => {
          visitUrl("clan_viplounge.php?action=fwshop&whichfloor=2");
          set("_loopuFireworksPrepped", true);
        },
      },
      {
        name: "Stillsuit Prep",
        completed: () => itemAmount($item`tiny stillsuit`) === 0,
        do: () =>
          equip(
            $item`tiny stillsuit`,
            get(
              "stillsuitFamiliar",
              $familiars`Gelatinous Cubeling`.find((fam) => have(fam)) ||
                $familiar`none`,
            ),
          ),
      },
      {
        name: "Run",
        completed: () => step("questL13Final") > 11,
        prepare: () => uneffect($effect`Beaten Up`),
        do: () => {
          cliExecute(args.smol);
          setRef("smolDone", true);
        },
        clear: "all",
        tracking: "run",
        limit: { tries: 2 },
      },
      {
        name: "drink",
        ready: () =>
          step("questL13Final") > 11 &&
          have($item`designer sweatpants`) &&
          have($skill`Drinking to Drink`) &&
          storageAmount($item`synthetic dog hair pill`) >= 1,
        completed: () => myInebriety() >= 2,
        do: (): void => {
          if (have($skill`The Ode to Booze`)) {
            useSkill($skill`The Ode to Booze`);
          }
          drink($item`astral pilsner`, 1);
        },
        clear: "all",
        tracking: "Run",
      },
      freeKing(),
      pullAll(),
      uneat(),
      {
        name: "Organ",
        completed: () => have($skill`Liver of Steel`),
        do: () => cliExecute("loopcasual goal=organ"),
        limit: { tries: 1 },
      },
    ],
  };
}

export function uneat(): Task {
  /*
  Smol has full organs, it's worth uneating
  */
  const task = {
    name: "Uneat",
    after: ["Pull All"],
    completed: () =>
      (getRemainingStomach() >= 0 && getRemainingLiver() >= 0) ||
      getRef("uneated") === "true",
    ready: () => getRef("uneated") !== "true",
    // biome-ignore lint/complexity/noExcessiveCognitiveComplexity: <explanation>
    do: (): void => {
      if (myFullness() >= 3 && myInebriety() >= 3 && !get("spiceMelangeUsed")) {
        if (!have($item`spice melange`)) {
          acquire(1, $item`spice melange`, 400000);
        }
        use($item`spice melange`);
      }
      if (
        getRemainingStomach() < 0 &&
        get("_augSkillsCast") < 5 &&
        !get("_aug16Cast")
      ) {
        useSkill($skill`Aug. 16th: Roller Coaster Day!`);
      }
      if (
        getRemainingStomach() < 0 &&
        have($item`distention pill`) &&
        !get("_distentionPillUsed")
      ) {
        use($item`distention pill`);
      }
      if (
        getRemainingLiver() < 0 &&
        have($item`synthetic dog hair pill`) &&
        !get("_syntheticDogHairPillUsed")
      ) {
        use($item`synthetic dog hair pill`);
      }
      if (getRemainingLiver() < 0 && !get("_sobrieTeaUsed")) {
        if (!have($item`cuppa Sobrie tea`)) {
          acquire(1, $item`cuppa Sobrie tea`, 100000);
        }
        use($item`cuppa Sobrie tea`);
      }
      setRef("uneated", true);
    },
    tracking: "uneat",
    limit: { tries: 1 },
  };
  return task;
}

export function smolAll(): Quest[] {
  return [prep(runSettings, pullSettings), smolRun(runSettings), postLoop()];
}

export function justSmol(): Quest[] {
  return [prep(runSettings, pullSettings), smolRun(runSettings)];
}
