import { step } from "grimoire-kolmafia";
import { cliExecute, myAdventures, myAscensions, visitUrl } from "kolmafia";
import {
  $class,
  $effect,
  $item,
  $path,
  get,
  have,
  set,
  uneffect,
} from "libram";
import { postLoop } from "../aftercore/post";
import { mayam, pullAll } from "../common";
import { Leg, type Quest, getCurrentLeg, inAscension } from "../structure";
import { setRef, totallyDrunk } from "../utils";
import { type PullSettings, type RunSettings, prep } from "./common";

const runSettings: RunSettings = {
  // path: $path`Standard`,
  path: $path`Quantum Terrarium`,
  // path: $path`Avant Guard`,
  class: $class`Seal Clubber`,
  moon: "Vole",
  pet: $item`astral mask`,
  consumable: $item`astral six-pack`,
  garden: "packet of rock seeds",
  workshed: "Model train set",
};

const pullSettings: PullSettings = {
  pulls: [],
  crafts: [
    // $item`Pizza of Legend`,
    // $item`Calzone of Legend`,
    // $item`Deep Dish of Legend`,
  ],
  buys: [],
};

/*
  Do an autoscend run, over multiple days potentiall
*/

export function stanardRun(runSettings: RunSettings): Quest {
  // Either we finished the run, or we're out of adventures and in the run
  const completion =
    (step("questL13Final") > 11 && getCurrentLeg() === Leg.PostRun) ||
    (getCurrentLeg() === Leg.InRun && myAdventures() >= 0 && totallyDrunk());
  return {
    name: runSettings.path.name,
    ready: () => inAscension(),
    completed: () => completion,
    tasks: [
      {
        name: "Refresh All",
        completed: () => get("_loopuRefreshed", false),
        do: () => {
          cliExecute("refresh all");
          set("_loopuRefreshed", true);
          mayam();
        },
      },
      {
        name: "Prep Fireworks Shop",
        completed: () =>
          !have($item`Clan VIP Lounge key`) ||
          get("_loopuFireworksPrepped", false),
        do: () => {
          visitUrl("clan_viplounge.php?action=fwshop&whichfloor=2");
          set("_loopuFireworksPrepped", true);
        },
      },
      {
        name: "Run",
        completed: () => get("lastEmptiedStorage") === myAscensions(),
        prepare: () => uneffect($effect`Beaten Up`),
        do: () => {
          cliExecute("autoscend");
          setRef("autoScendDone", true);
        },
        clear: "all",
        tracking: "run",
        limit: { tries: 2 },
      },
      pullAll(),
    ],
  };
}

export function standardAll(): Quest[] {
  return [prep(runSettings, pullSettings), stanardRun(runSettings), postLoop()];
}

export function justStandard(): Quest[] {
  return [prep(runSettings, pullSettings), stanardRun(runSettings)];
}
