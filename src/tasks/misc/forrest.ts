import { CombatStrategy } from "grimoire-kolmafia";
import { useFamiliar, visitUrl } from "kolmafia";
import { $familiar, $item, $location, Macro, get, have } from "libram";
import type { Task } from "../structure";

/*
Unlock the forest
 */

function combat() {
  return new Macro()
    .tryItem($item`Tattered scrap of paper`)
    .tryItem($item`Tattered scrap of paper`)
    .repeat();
}

export function UnlockForest(): Task[] {
  return [
    {
      name: "Open Guild",
      completed: () => get("questG07Myst") === "started",
      do: () => {
        // Potentially add logic for bacon machine?
        visitUrl("guild.php?place=challenge");
      },
      limit: { tries: 1 },
    },
    {
      name: "Unlock Guild",
      do: $location`The Haunted Pantry`,
      choices: { 115: 1, 116: 4, 117: 1, 114: 2 },
      completed: () => have($item`exorcised sandwich`),
      prepare: () => useFamiliar($familiar`Gelatinous Cubeling`),
      combat: new CombatStrategy().autoattack(combat),
      outfit: {
        modifier: "combat",
      },
      limit: { turns: 7 },
    },
  ];
}
