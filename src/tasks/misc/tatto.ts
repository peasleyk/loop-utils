import { CombatStrategy } from "grimoire-kolmafia";
import {
  type Monster,
  cliExecute,
  haveEffect,
  itemAmount,
  itemDropModifier,
  use,
  useFamiliar,
} from "kolmafia";
import {
  $effect,
  $familiar,
  $item,
  $monster,
  $skill,
  Macro,
  get,
  have,
} from "libram";
import type { Quest } from "../structure";
import { heal } from "../utils";

/*

Script to Farm tatters

TODO:
  Make sure we buff up to 5 famw eight
  Ensure we have 11 fam exp
  Handle auton-aton
  Handle ptrack
  Handle Cleaver
  Handle Sausages
  Handle libram burning
  Handle buff upkeep
    - get list of buffs
  Initial structure
  Banishing monsters
  Olfaction
  Pledge allegiance


*/

const ITEM_TARGET = 700;
const RUN_AWAY_ITEM = $item`GOTO`;
const RUN_AWAY_AMOUNT = 30;

function dinerCombat(toFarm: Monster, toSkip: Monster) {
  /*
    Farm items from the diner, skip the cocktail
  */
  const killSKill = $skill`Shrap`;
  const cocktail = $monster`Sloppy Seconds Cocktail`;
  return new Macro()
    .trySkill($skill`Sing Along`)
    .trySkill($skill`Bowl Straight Up`)
    .tryItem($item`porquoise-handled sixgun`)
    .trySkill($skill`Emit Matter Duplicating Drones`)
    .if_(toFarm, Macro.trySkill($skill`Transcendent Olfaction`))
    .if_(toSkip, Macro.item(RUN_AWAY_ITEM, RUN_AWAY_ITEM).repeat())
    .if_(cocktail, Macro.item(RUN_AWAY_ITEM, RUN_AWAY_ITEM).repeat())
    .skill(killSKill)
    .repeat();
}

export function Tatto(): Quest {
  const toFarm = $monster`Sloppy Seconds Sundae`;
  const toSkip = $monster`Sloppy Seconds Burger`;

  return {
    name: "Farm Tatters",
    completed: () => get("garbageChampagneCharge") === 1,
    tasks: [
      {
        name: "Prep Goose",
        completed: () =>
          get("_garbageItemChanged") === true &&
          haveEffect($effect`Steamed Sinuses`) > 1,
        acquire: [
          {
            item: $item`pulled blue taffy`,
            price: 4000,
            optional: false,
            num: 1,
            useful: () => {
              return (
                itemAmount($item`pulled blue taffy`) === 0 &&
                haveEffect($effect`Blue Swayed`) === 0
              );
            },
          },
          {
            item: $item`ghost dog chow`,
            price: 3000,
            optional: true,
            num: 3,
            useful: () => {
              return $familiar`Grey Goose`.experience < 36;
            },
          },
        ],
        do: () => {
          cliExecute("maximize item");
          useFamiliar($familiar`Grey Goose`);

          if (have($item`pulled blue taffy`)) {
            use($item`pulled blue taffy`);
          }
          let count = 0;
          while (
            $familiar`Grey Goose`.experience < 36 &&
            have($item`ghost dog chow`)
          ) {
            use($item`ghost dog chow`);
            count += 1;
            if (count > 3) {
              throw "Looped too many times using chow, something is up";
            }
          }
        },
        limit: { tries: 1 },
        tracking: "Prep",
      },
      {
        name: "Blah",
        prepare: () => heal(0.5),
        ready: () => {
          return true;
        },
        completed: () => true,
        do: () => {},
        outfit: {
          weapon: $item`yule hatchet`,
          familiar: $familiar`Grey Goose`,
          famequip: $item`grey down vest`,
          acc1: $item`teacher's pen`,
          acc2: $item`teacher's pen`,
          modifier: "item",
        },
        limit: { turns: 1 },
        tracking: "Blah",
      },
      {
        name: "Farm",
        after: ["Prep"],
        prepare: () => heal(0.5),
        ready: () => itemDropModifier() >= ITEM_TARGET,
        completed: () => get("garbageChampagneCharge") === 1,
        do: () => {},
        combat: new CombatStrategy().autoattack(dinerCombat(toFarm, toSkip)),
        outfit: {
          weapon: $item`yule hatchet`,
          familiar: $familiar`Grey Goose`,
          famequip: $item`grey down vest`,
          acc1: $item`teacher's pen`,
          acc2: $item`teacher's pen`,
          modifier: "item",
        },
        limit: { turns: 1 },
        tracking: "Farm",
      },
    ],
  };
}
