import type {
  Quest as BaseQuest,
  Task as BaseTask,
  Limit,
} from "grimoire-kolmafia";
import { canInteract, myPath, myStorageMeat } from "kolmafia";
import { get } from "libram";

export type Task = BaseTask & {
  tracking?: string;
  limit?: Limit;
  clear?: "all" | "outfit" | "macro" | ("outfit" | "macro")[];
};
export type Quest = BaseQuest<Task>;

export enum Leg {
  PreRun = 0,
  InRun = 1,
  PostRun = 2,
}

export function getCurrentLeg(): number {
  if (preAscension()) {
    return Leg.PreRun;
  }
  if (inAscension()) {
    return Leg.InRun;
  }
  if (hasAscended()) {
    return Leg.PostRun;
  }
  throw new Error("Somehow we're in an unknown leg");
}

// 0 = aftercore
export function inAscension(): boolean {
  return myPath().id !== 0; // get("lastEmptiedStorage") !== myAscensions();
}

// Used for post run activites
export function hasAscended(): boolean {
  return get("ascensionsToday") === 1 && canInteract() && myStorageMeat() === 0;
}

// If we're not in an ascension, and we haven't ascemded yet, then
// we're adventuring pre-ascension
export function preAscension(): boolean {
  return !(inAscension() || hasAscended());
}
