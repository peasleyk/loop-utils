import { getRef } from "./utils";

/*

Used for testing different tasks before integrating

*/
import {
  currentPvpStances,
  getCampground,
  itemAmount,
  mallPrice,
  print,
} from "kolmafia";
import { $familiar, $item, get } from "libram";

type MallPrice = {
  formattedMallPrice: string;
  formattedLimitedMallPrice: string;
  formattedMinPrice: string;
  mallPrice: number;
  limitedMallPrice: number;
  minPrice: number | null;
};

// https://github.com/loathers/oaf/blob/ec6929c802cdcc31e35e85aa58c571f9e1af5fdc/src/clients/kol.ts#L399
export function test3() {
  print(getRef("ptrackStarted"));
  if (getRef("ptrackStarted") === "true") {
    print("It's true");
  }
}

// Pull PVP logs
export function test(): void {
  const leaves = new Map([
    [$item`autumnic bomb`, 37],
    [$item`distilled resin`, 50],
    [$item`autumnal aegis`, 66],
    [$item`forest canopy bed`, 74],
    [$item`autumnic balm`, 99],
    [$item`coping juice`, 1111],
    [$item`smoldering leafcutter ant egg`, 6666],
    [$item`super-heated leaf`, 11111],
  ]);

  const total = itemAmount($item`inflammable leaf`);
  let maxProfit = 0;
  let toBuy;
  for (const [it, cost] of leaves) {
    const profit = mallPrice(it) * Math.floor(total / cost);
    if (profit > maxProfit) {
      maxProfit = profit;
      toBuy = it;
    }
    print(`${it.toString()}: ${profit}`);
  }
  print(`Best: ${toBuy.toString()}, ${maxProfit}`);
}

// Record things
function test2(stance?: string) {
  if (stance === null || stance === undefined) {
    const stances = currentPvpStances();
    print(Object.keys(stances)[0]);
    // stance = stances.keys[0];
  }
  print(getCampground()["Clockwork maid"]);
  for (const key in Object.values(getCampground())) {
    // print(key);
    print(getCampground()["Dramatic™ range"]);
    // print(Object.values(getCampground())[key]);
  }
  print($familiar`Grey Goose`.experience);
  print(get("garbageChampagneCharge"));
}
